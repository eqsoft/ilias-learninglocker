# Learning Locker version 2 for ILIAS docker family

Do not use without further information: [schneider@hrz.uni-marburg.de](mailto:schneider@hrz.uni-marburg.de)

- Official Learning Locker Repoistory: [https://github.com/LearningLocker/learninglocker](https://github.com/LearningLocker/learninglocker)
- A dockerized version: [https://hub.docker.com/u/learninglocker](https://hub.docker.com/u/learninglocker)
